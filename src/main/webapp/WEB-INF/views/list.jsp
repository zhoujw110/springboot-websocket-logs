<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach var="item" items="${deployList}">
    <tr>
        <td>${item.uuid}</td>
        <td>${item.name}</td>
        <td>${item.host}</td>
        <td>${item.logPath}</td>
        <td><a href="${pageContext.request.contextPath}/logs/del/${item.uuid}?env=${item.env} "  class="btn waves-effect waves-light lighten-2"> 删除</a></td>
        <td><a href="${pageContext.request.contextPath}/logs/${item.uuid}"  class="btn waves-effect waves-light lighten-2" target="_blank" >查看日志</a></td>

    </tr>
</c:forEach>
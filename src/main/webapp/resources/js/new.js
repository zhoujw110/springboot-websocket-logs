function layerAlert(text) {
    $("#alert-modal .text-alert").html(text);
    $("#alert-modal").openModal({dismissible: false});
}
function addNode () {
    var name = $("#input-name").val();
    var host = $("#input-host").val();
    var port = $("#input-port").val();
    var profile = $("#input-profile").val();

    var user = $("#input-user").val();
    var logPath = $("#input-logPath").val();

    if(name.length === 0) {
        layerAlert("请填写项目名称！")
        return false;
    }
    if(host.length === 0) {
        layerAlert("请填写监控目标Host或者IP！")
        return false;
    }
    if(profile.length === 0) {
        layerAlert("请填写所属环境！")
        return false;
    }
    if(user.length === 0) {
        layerAlert("请填写免密执行shell的用户！")
        return false;
    }
    if(logPath.length === 0) {
        layerAlert("请填写监控日志文件地址！")
        return false;
    }
    if(parseInt(port).toString() !== port || parseInt(port) < 0 || parseInt(port) > 65536) {
        layerAlert("请填写正确的端口号！")
        return false;
    }
    /**
     * ajax请求后台运行脚本
     */
    ajaxShell("../config/add");
    //layerAlert("提交表单~~~~~！")

};

function ajaxShell(url) {
   // $("#loader-modal").openModal({dismissible: false});
    $.ajax({
        url: url,
        type: "POST",
        data: new FormData($('#form-new')[0]),
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
        	if(data.success){
                //$("#alert-modal .text-alert").html("成功~");
                layerAlert("添加成功~~~~~！")

			} else {
                layerAlert("添加失败~~~~~！")
			}
        },
        error: function () {
            //$("#loader-modal").closeModal();
            layerAlert("发生异常，请重试！");
        }
    });
}

function toList () {
    window.history.go(-1);
}

